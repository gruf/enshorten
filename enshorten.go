package enshorten

import (
	"errors"
	"fmt"
	"time"

	"codeberg.org/gruf/enshorten/models"
	"codeberg.org/gruf/enshorten/store"
	"codeberg.org/gruf/enshorten/strgen"
	"codeberg.org/gruf/go-logger"
	"codeberg.org/gruf/go-middleware"
	"codeberg.org/gruf/go-store/storage"
	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

// rootHelpStr is the root page help string printed (before formatting).
var rootHelpStr = `Enshorten -- a URL shortening service!

Usage:
$ curl https://%s/s?url=<URL>
--> '%s/<KEY>'
$ curl https://%s/<KEY>
--> redirects you to <URL>
`

type Config struct {
	// KeyLen is the string length of generated shortened URL keys.
	KeyLen uint

	// Address is the access URL addr of this server, used in generating root help page.
	Address string
}

type Enshorten struct {
	help   string
	cfg    Config
	store  *store.URLStore
	router *router.Router
	handle fasthttp.RequestHandler
}

// New returns a new Enshorten instance using supplied configuration.
func New(store *store.URLStore, cfg Config) *Enshorten {
	// Create the application
	app := &Enshorten{
		help: fmt.Sprintf(
			rootHelpStr,
			cfg.Address,
			cfg.Address,
			cfg.Address,
		),
		cfg:    cfg,
		store:  store,
		router: router.New(),
	}

	// Setup panic and logging handler
	app.handle = middleware.Logger(logger.Default(), app.router.Handler)
	app.router.PanicHandler = func(ctx *fasthttp.RequestCtx, i interface{}) {
		middleware.PanicHandler(logger.Default(), ctx, i)
	}

	// Register application HTTP routes
	app.router.GET("/", app.Root)
	app.router.GET("/s", app.CreateShortURL)
	app.router.GET("/{key}", app.FollowShortURL)

	return app
}

// Handler implements fasthttp.RequestHandler for Enshorten.
func (en *Enshorten) Handler(ctx *fasthttp.RequestCtx) {
	en.handle(ctx)
}

// Root is the HTTP handler for the root route (heh).
func (en *Enshorten) Root(ctx *fasthttp.RequestCtx) {
	ctx.SetStatusCode(200)
	ctx.WriteString(en.help)
}

// FollowShortURL is the HTTP handler for the 'follow-short-URL' route.
func (en *Enshorten) FollowShortURL(ctx *fasthttp.RequestCtx) {
	// Get URL shortkey from path param
	key, _ := ctx.UserValue("key").(string)

	// Look for ShortURL from the store
	url, err := en.store.GetShortened(key)
	if err != nil {
		// Long URL for shortkey cannot be found
		if errors.Is(err, storage.ErrNotFound) {
			ctx.Error("URL Not Found", fasthttp.StatusNotFound)
			return
		}

		// User provided us an invalid shortkey
		if errors.Is(err, models.ErrInvalidShortKey) {
			ctx.Error("Invalid Short URL Key", fasthttp.StatusBadRequest)
			return
		}

		// All other errors we mask and instead log them ourselves
		ctx.Error("Internal Server Error", fasthttp.StatusInternalServerError)
		logger.Error(err)
		return
	}

	// Redirect the user to provided long URL
	ctx.Redirect(url.LongURL, fasthttp.StatusFound)
}

// CreateShortURL is the HTTP handler for creating a shortened URL.
func (en *Enshorten) CreateShortURL(ctx *fasthttp.RequestCtx) {
	// Get the long URL from query param
	longURL := ctx.QueryArgs().Peek("url")

	// Prepare the ShortURL object
	url := models.ShortURL{
		ShortKey: strgen.RandomString(en.cfg.KeyLen),
		LongURL:  string(longURL),
		Created:  time.Now(),
	}

	// Attempt to place to place this in the store
	if err := en.store.PutShortened(url); err != nil {
		// Provided long URL from user was invalid
		if errors.Is(err, models.ErrInvalidLongURL) {
			ctx.Error("Invalid URL", fasthttp.StatusBadRequest)
			return
		}

		// Catch other unrecoverable error from store
		if !errors.Is(err, models.ErrInvalidShortKey) {
			ctx.Error("Internal Server Error", fasthttp.StatusInternalServerError)
			logger.Error(err)
			return
		}

		// This could be a key-conflict, make 1 last attempt
		url.ShortKey = strgen.RandomString(en.cfg.KeyLen)
		if err := en.store.PutShortened(url); err != nil {
			ctx.Error("Internal Server Error", fasthttp.StatusInternalServerError)
			logger.Error(err)
			return
		}
	}

	// Return the generated URL string in return
	ctx.SetStatusCode(200 /*all good */)
	ctx.WriteString(en.cfg.Address + "/" + url.ShortKey)
}
