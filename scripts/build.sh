#!/bin/sh

set -e

# Get git commit hash
COMMIT=$(git rev-parse --short HEAD)

# Determine version str
VERSION=$(git name-rev --name-only ${COMMIT})
VERSION=${VERSION#tags/}

CGO_ENABLED=0 \
go build -trimpath -v \
         -tags 'netgo osusergo static_build' \
         -ldflags "-s -w -extldflags '-static' -X 'main.Commit=${COMMIT}' -X 'main.Version=${VERSION}'" \
         -gcflags '-l=4' \
         -o 'enshorten' \
         ./cmd/enshorten
