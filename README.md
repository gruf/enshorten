# Enshorten

A cURL friendly URL shortening service!

`scripts/build.sh` to build the binary

`go test -v ./...` to run all tests

`scripts/enshorten-cli` for a short CLI tool

See root help page for usage instructions.