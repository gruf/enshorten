package store

import (
	"strconv"
	"strings"
	"time"

	"codeberg.org/gruf/enshorten/models"
	"codeberg.org/gruf/go-store/object"
	"codeberg.org/gruf/go-store/storage"
)

type URLStore struct {
	// NOTE: we use ObjectStore here to store the ShortURL objects
	//       as it automatically handles encoding/decoding of these
	//       objects to data in-memory/on-disk and caching of encoded
	store *object.ObjectStore
}

func Open(path string) (*URLStore, error) {
	var st storage.Storage

	if strings.HasPrefix(path, ":memory:") && !strings.Contains(path, "/") {
		// This is the pecial case of in-memory store

		// Attempt to parse a provided size string
		size := 100 // default
		if i := strings.IndexByte(path, '?'); i != -1 {
			size, _ = strconv.Atoi(path[i+1:])
			if size < 1 {
				size = 100
			}
		}

		// Open the in-memory storage
		st = storage.OpenMemory(size, false)
	} else {
		// All else are filesystem paths, open disk
		disk, err := storage.OpenFile(path, nil)
		if err != nil {
			return nil, err
		}

		// Set disk store
		st = disk
	}

	// Open the object storage
	store, err := object.OpenStorage(st, time.Hour)
	if err != nil {
		return nil, err
	}

	return &URLStore{store}, nil
}

// GetShortened looks for a ShortURL within the store with given short key.
func (store *URLStore) GetShortened(key string) (*models.ShortURL, error) {
	// Check for a valid input key
	if key == "" {
		return nil, models.ErrInvalidShortKey
	}

	// Allocate the return URL
	url := models.ShortURL{}

	// Look for the ShortURL in the store
	if err := store.store.Get(key, &url); err != nil {
		return nil, err
	}

	return &url, nil
}

// PutShortened places provided ShortURL within the store, checking for validity before input.
func (store *URLStore) PutShortened(url models.ShortURL) error {
	// Validate the input short URL
	if err := url.Validate(); err != nil {
		return err
	}

	// Check for shortkey collision (count as invalid key)
	if ok, err := store.store.Has(url.ShortKey); err != nil {
		return err
	} else if ok {
		return models.ErrInvalidShortKey
	}

	// Store the ShortURL under short key
	return store.store.Put(url.ShortKey, &url)
}

// Prune will remove all ShortURLs from the store older than 'age', returning number cleaned.
func (store *URLStore) Prune(age time.Duration) (int, error) {
	// Get a store iterator for all the URLs
	iter, err := store.store.Iterator(nil)
	if err != nil {
		return 0, err
	}

	// Calculate oldest allowed time
	oldest := time.Now().Add(-age)

	// Track the keys to delete
	url := models.ShortURL{}
	toDelete := []string{}

	for iter.Next() {
		// Read the current value into 'url'
		if err := iter.Value(&url); err != nil {
			iter.Release() // unlocks store
			return 0, err
		}

		// Check for 'url' expiration (if so add
		// to the slice of URL keys to be deleted)
		if url.Created.Before(oldest) {
			toDelete = append(toDelete, url.ShortKey)
		}
	}

	// Done with iterator
	iter.Release()

	// Count deleted
	count := 0

	// Delete all of the expired keys
	for _, key := range toDelete {
		if store.store.Delete(key) == nil {
			count++
		}
	}

	return count, nil
}
