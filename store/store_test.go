package store_test

import (
	"reflect"
	"testing"
	"time"

	"codeberg.org/gruf/enshorten/models"
	"codeberg.org/gruf/enshorten/store"
	"codeberg.org/gruf/enshorten/strgen"
)

func init() {
	for i := 0; i < 1000; i++ {
		key := strgen.RandomString(8)
		testURLs[key] = models.ShortURL{
			ShortKey: key,
			LongURL:  "https://google.com",
			Created:  time.Now().Add(-time.Hour),
		}
	}
}

// testURLs contains generated test URL strings.
var testURLs = map[string]models.ShortURL{}

func TestStore(t *testing.T) {
	// Open an in-memory store for testing
	store, err := store.Open(":memory:?size=100")
	if err != nil {
		t.Fatal("failed opening store")
	}

	// Add each of the test URLs to the store
	for _, url := range testURLs {
		if err := store.PutShortened(url); err != nil {
			t.Fatal("failed putting ShortURL in store:", err)
		}
	}

	// Ensure each of the URLs is in the store
	for key, expect := range testURLs {
		url, err := store.GetShortened(key)
		if err != nil {
			t.Fatal("failed getting ShortURL from store:", err)
		} else if !reflect.DeepEqual(url, &expect) {
			t.Fatal("failed ensuring retrieved URL is as expected")
		}
	}

	// Clean all these entries from store
	count, err := store.Prune(time.Second /* should remove all */)
	if err != nil {
		t.Fatal("failed removing old ShortURLs from store:", err)
	} else if count != len(testURLs) {
		t.Fatal("failed cleaning expected number of ShortURLs from store:", err)
	}
}
