package models

import "net/url"

// isValidURL checks if 'u' is a valid HTTP/HTTPS URL string.
func isValidURL(u string) bool {
	_, err := url.Parse(u)
	return err == nil
}
