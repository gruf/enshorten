package models

import (
	"encoding/json"
	"errors"
	"net/url"
	"time"

	"codeberg.org/gruf/go-store/object"
)

var (
	// ErrInvalidShortKey is returned when ShortURL.ShortKey is empty.
	ErrInvalidShortKey = errors.New("invalid URL short key")

	// ErrInvalidLongURL is returned when ShortURL.LongURL is invalid (empty or bad URL).
	ErrInvalidLongURL = errors.New("invalid URL long url")

	// errUnsetTime is returned when ShortURL.Created is unset.
	errUnsetTime = errors.New("unset created time")

	// errInvalidObjectType is returned if .Set() is attempted with wrong supplied type.
	errInvalidObjectType = errors.New("invalid object.Object type")

	// errNilReciver is returned if .Unmarshal() is attempted on a nil reciver.
	errNilReceiver = errors.New("nil receiving ShortURL")
)

type ShortURL struct {
	// ShortKey is the short key string used to store the URL
	// and used as the path in the shortened result URL.
	ShortKey string

	// LongURL is the provided long URL to be shortened.
	LongURL string

	// Created is the date of creation of this shortening.
	Created time.Time
}

// Validate ensures a valid ShortURL
func (u *ShortURL) Validate() error {
	// Catch fast invalid cases
	switch {
	case u.ShortKey == "":
		return ErrInvalidShortKey
	case u.LongURL == "":
		return ErrInvalidLongURL
	case u.Created.IsZero():
		return errUnsetTime
	}

	// Check the LongURL
	url, err := url.Parse(u.LongURL)
	if err != nil {
		return ErrInvalidLongURL
	}

	// Ensure a scheme is set (and valid!)
	if url.Scheme != "http" && url.Scheme != "https" {
		return ErrInvalidLongURL
	} else if url.Scheme == "" {
		url.Scheme = "https"
		u.LongURL = url.String()
	}

	return nil
}

// Copy implements object.Object's .Copy()
func (u *ShortURL) Copy() object.Object {
	return &ShortURL{
		ShortKey: u.ShortKey,
		LongURL:  u.LongURL,
		Created:  u.Created,
	}
}

// Set implements object.Object's .Set()
func (u *ShortURL) Set(obj object.Object) error {
	set, ok := obj.(*ShortURL)
	if !ok || set == nil {
		return errInvalidObjectType
	}
	u.ShortKey = set.ShortKey
	u.LongURL = set.LongURL
	u.Created = set.Created
	return nil
}

// Marshal implements object.Object's .Marshal()
func (u *ShortURL) Marshal() ([]byte, error) {
	return json.MarshalIndent(u, "", "\t")
}

// Unmarshal implements object.Object's .Unmarshal()
func (u *ShortURL) Unmarshal(b []byte) error {
	if u == nil {
		return errNilReceiver
	}
	return json.Unmarshal(b, u)
}
