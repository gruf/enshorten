package models_test

import (
	"errors"
	"testing"
	"time"

	"codeberg.org/gruf/enshorten/models"
)

var tests = []struct {
	model models.ShortURL
	err   error
}{
	{
		model: models.ShortURL{
			ShortKey: "key",
			LongURL:  "https://google.com",
			Created:  time.Now(),
		},
		err: nil,
	},
	{
		model: models.ShortURL{
			ShortKey: "",
			LongURL:  "https://google.com",
			Created:  time.Now(),
		},
		err: models.ErrInvalidShortKey,
	},
	{
		model: models.ShortURL{
			ShortKey: "key",
			LongURL:  ":?badurlstring",
			Created:  time.Now(),
		},
		err: models.ErrInvalidLongURL,
	},
	{
		model: models.ShortURL{
			ShortKey: "key",
			LongURL:  "",
			Created:  time.Now(),
		},
		err: models.ErrInvalidLongURL,
	},
	{
		model: models.ShortURL{
			ShortKey: "key",
			LongURL:  "https://google.com",
			Created:  time.Time{},
		},
		err: errors.New("unset created time"),
	},
}

func TestShortURL(t *testing.T) {
	for _, test := range tests {
		// Validate the test model
		err := test.model.Validate()

		// Check error results are expcted
		if test.err == nil && err != nil {
			t.Fatal("validate returned unexpected error")
		} else if test.err != nil && (test.err.Error() != err.Error() || err == nil) {
			t.Fatal("validate did not return expected error string", err)
		}
	}
}
