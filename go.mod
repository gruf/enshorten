module codeberg.org/gruf/enshorten

go 1.17

require (
	codeberg.org/gruf/go-logger v1.4.1
	codeberg.org/gruf/go-middleware v0.1.6
	codeberg.org/gruf/go-store v1.3.1
	github.com/fasthttp/router v1.4.5
	github.com/spf13/pflag v1.0.5
	github.com/valyala/fasthttp v1.32.0
)

require (
	codeberg.org/gruf/go-bytes v1.0.2 // indirect
	codeberg.org/gruf/go-cache v1.1.2 // indirect
	codeberg.org/gruf/go-errors v1.0.5 // indirect
	codeberg.org/gruf/go-fastpath v1.0.2 // indirect
	codeberg.org/gruf/go-format v1.0.3 // indirect
	codeberg.org/gruf/go-hashenc v1.0.2 // indirect
	codeberg.org/gruf/go-mutexes v1.1.0 // indirect
	codeberg.org/gruf/go-nowish v1.1.0 // indirect
	codeberg.org/gruf/go-pools v1.0.2 // indirect
	codeberg.org/gruf/go-runners v1.2.0 // indirect
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/klauspost/compress v1.14.1 // indirect
	github.com/savsgio/gotils v0.0.0-20211223103454-d0aaa54c5899 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
)
