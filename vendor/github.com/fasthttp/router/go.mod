module github.com/fasthttp/router

go 1.15

require (
	github.com/savsgio/gotils v0.0.0-20211223103454-d0aaa54c5899
	github.com/valyala/bytebufferpool v1.0.0
	github.com/valyala/fasthttp v1.32.0
)
