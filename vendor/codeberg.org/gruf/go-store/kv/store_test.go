package kv_test

import (
	"fmt"
	"io"
	"os"
	"strings"
	"testing"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-store/kv"
	"codeberg.org/gruf/go-store/storage"
)

func dataf(s string, a ...interface{}) []byte {
	return bytes.StringToBytes(fmt.Sprintf(s, a...))
}

func TestKVStoreDisk(t *testing.T) {
	// Set test path, defer deleting it
	testPath := "kvstore.test_disk"
	t.Cleanup(func() {
		os.RemoveAll(testPath)
	})

	// Open DiskStorage
	disk, err := storage.OpenFile(testPath, nil)
	if err != nil {
		t.Fatalf("Error opening storage: %v", err)
	}

	// Test the KVStore
	testKVStore(t, disk)
}

func TestKVStoreBlock(t *testing.T) {
	// Set test path, defer deleting it
	testPath := "kvstore.test_block"
	t.Cleanup(func() {
		os.RemoveAll(testPath)
	})

	// Open BlockStorage
	block, err := storage.OpenBlock(testPath, nil)
	if err != nil {
		t.Fatalf("Error opening storage: %v", err)
	}

	// Test the KVStore
	testKVStore(t, block)
}

func TestKVStoreMemory(t *testing.T) {
	// Open MemoryStorage
	mem := storage.OpenMemory(100, false)

	// Test the KVStore
	testKVStore(t, mem)
}

func testKVStore(t *testing.T, st storage.Storage) {
	expected := map[string][]byte{
		"test1": dataf("hello world!"),
		"test2": dataf("hello world!\nhelloworld!\nhelloworld!\nhewwoooo wooowddd\n"),
		"test3": dataf("i dunno some random data here %d", 64),
		"test4": dataf("aaaaaaaaaaaaaaaa asfj;asdflkjasd;kfnasdklf"),
	}

	// Open KVStore
	kv, err := kv.OpenStorage(st)
	if err != nil {
		t.Fatalf("Error opening store: %v", err)
	}

	// Write bytes first to give something for later reads
	t.Run("KVStore.Put()", func(t *testing.T) {
		testKVStorePut(t, kv, "test1", expected["test1"])
		testKVStorePut(t, kv, "test2", expected["test2"])
	})
	t.Run("KVStore.PutStream()", func(t *testing.T) {
		testKVStorePutStream(t, kv, "test3", expected["test3"])
		testKVStorePutStream(t, kv, "test4", expected["test4"])
	})

	// Now attempt to read from previous tests
	t.Run("KVStore.Get()", func(t *testing.T) {
		testKVStoreGet(t, kv, "test1", expected["test1"])
		testKVStoreGet(t, kv, "test2", expected["test2"])
	})
	t.Run("KVStore.GetStream()", func(t *testing.T) {
		testKVStoreGetStream(t, kv, "test3", expected["test3"])
		testKVStoreGetStream(t, kv, "test4", expected["test4"])
	})

	// Check that files from previous tests exist
	t.Run("KVStore.Has()", func(t *testing.T) {
		for key := range expected {
			testKVStoreHas(t, kv, key)
		}
	})

	// Attempt to remove 1 of files from previous test
	t.Run("KVStore.Delete()", func(t *testing.T) {
		testKVStoreDelete(t, kv, "test1")
		delete(expected, "test1")
	})

	// Iterate keys and check expected
	t.Run("KVStore.Iterator()", func(t *testing.T) {
		testKVStoreIterator(t, kv, expected)
	})

	// Check concurrent reads
	t.Run("KVStore.GetStream() [concurrent]", func(t *testing.T) {
		testKVStoreConcurrentReads(t, kv, expected)
	})

	// Check concurrent writes
	t.Run("KVStore.PutStream() [concurrent]", func(t *testing.T) {
		testKVStoreConcurrentWrites(t, kv)
	})

	t.Run("KVStore.Read()", func(t *testing.T) {})
	t.Run("KVStore.Update()", func(t *testing.T) {})

	// Check closing store
	t.Run("KVStore.Close()", func(t *testing.T) {
		testKVStoreClose(t, kv)
	})
}

func testKVStoreGet(t *testing.T, kv *kv.KVStore, key string, expect []byte) {
	v, err := kv.Get(key)
	if err != nil {
		t.Fatalf("Error getting value from store: %v", err)
	}
	if !bytes.Equal(v, expect) {
		t.Fatalf("Error getting expected value '%s' from storage '%s'", expect, v)
	}
}

func testKVStoreGetStream(t *testing.T, kv *kv.KVStore, key string, expect []byte) {
	rc, err := kv.GetStream(key)
	if err != nil {
		t.Fatalf("Error getting value stream from store: %v", err)
	}
	defer rc.Close()
	v, err := io.ReadAll(rc)
	if err != nil {
		t.Fatalf("Error reading value from stream: %v", err)
	}
	if !bytes.Equal(v, expect) {
		t.Fatalf("Error getting expected value '%s' from storage '%s'", expect, v)
	}
}

func testKVStorePut(t *testing.T, kv *kv.KVStore, key string, value []byte) {
	err := kv.Put(key, value)
	if err != nil {
		t.Fatalf("Error putting value in store: %v", err)
	}
}

func testKVStorePutStream(t *testing.T, kv *kv.KVStore, key string, value []byte) {
	err := kv.PutStream(key, bytes.NewReader(value))
	if err != nil {
		t.Fatalf("Error putting value stream in store: %v", err)
	}
}

func testKVStoreHas(t *testing.T, kv *kv.KVStore, key string) {
	ok, err := kv.Has(key)
	if err != nil {
		t.Fatalf("Error checking if key in store: %v", err)
	}
	if !ok {
		t.Fatalf("Error checking expected key in store '%s'", key)
	}
}

func testKVStoreDelete(t *testing.T, kv *kv.KVStore, key string) {
	err := kv.Delete(key)
	if err != nil {
		t.Fatalf("Error deleting key from store: %v", err)
	}
}

func testKVStoreIterator(t *testing.T, kv *kv.KVStore, expected map[string][]byte) {
	iter, err := kv.Iterator(nil)
	if err != nil {
		t.Fatalf("Error getting store iterator: %v", err)
	}
	defer iter.Release()

	count := 0
	for iter.Next() {
		key := iter.Key()
		if _, ok := expected[key]; !ok {
			t.Fatalf("Error finding unexpected key in store '%s'", key)
		}
		value, err := iter.Value()
		if err != nil {
			t.Fatalf("Error getting value from store iterator: %v", err)
		}
		if !bytes.Equal(value, expected[key]) {
			t.Fatalf("Error checking key with '%s' has expected value '%s'", value, expected[key])
		}
		count++
	}

	if count != len(expected) {
		t.Fatalf("Unexpected number of keys in store '%d'", count)
	}
}

func testKVStoreConcurrentReads(t *testing.T, kv *kv.KVStore, expected map[string][]byte) {
	for key := range expected {
		rc, err := kv.GetStream(key)
		if err != nil {
			t.Fatalf("Error getting value stream from store: %v", err)
		}
		defer rc.Close()
	}
}

func testKVStoreConcurrentWrites(t *testing.T, kv *kv.KVStore) {
	rs := []*testReader{
		{
			r: strings.NewReader("1"),
			c: make(chan struct{}),
		},
		{
			r: strings.NewReader("2"),
			c: make(chan struct{}),
		},
		{
			r: strings.NewReader("3"),
			c: make(chan struct{}),
		},
		{
			r: strings.NewReader("4"),
			c: make(chan struct{}),
		},
		{
			r: strings.NewReader("5"),
			c: make(chan struct{}),
		},
	}

	chs := []chan struct{}{}
	for i, r := range rs {
		i, r := i, r // rescope
		ch := make(chan struct{})
		go func() {
			ch <- struct{}{}
			err := kv.PutStream(fmt.Sprintf("unexpected%d", i), r)
			if err != nil {
				panic("Error putting value stream in store" + err.Error())
			}
			ch <- struct{}{}
		}()
		<-ch
		chs = append(chs, ch)
	}

	for _, r := range rs {
		r.Release()
	}

	for _, ch := range chs {
		<-ch
	}
}

func testKVStoreClose(t *testing.T, kv *kv.KVStore) {
	err := kv.Close()
	if err != nil {
		t.Fatalf("Error closing store: %v", err)
	}
	_, err = kv.Get("doesn't matter")
	if err != storage.ErrClosed {
		t.Fatalf("Did not return ErrClosed after storage closed: %v", err)
	}
}

type testReader struct {
	r io.Reader
	c chan struct{}
}

func (r *testReader) Read(b []byte) (int, error) {
	<-r.c
	return r.r.Read(b)
}

func (r *testReader) Release() {
	close(r.c)
}
