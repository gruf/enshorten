package object_test

import (
	"errors"
	"fmt"
	"os"
	"testing"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-store/object"
	"codeberg.org/gruf/go-store/storage"
)

func TestObjectStoreDisk(t *testing.T) {
	// Set test path, defer deleting it
	testPath := "objectstore.test_disk"
	t.Cleanup(func() {
		os.RemoveAll(testPath)
	})

	// Open DiskStorage
	disk, err := storage.OpenFile(testPath, nil)
	if err != nil {
		t.Fatalf("Error opening storage: %v", err)
	}

	// Test the ObjectStore
	testObjectStore(t, disk)
}

func TestObjectStoreBlock(t *testing.T) {
	// Set test path, defer deleting it
	testPath := "objectstore.test_block"
	t.Cleanup(func() {
		os.RemoveAll(testPath)
	})

	// Open BlockStorage
	block, err := storage.OpenBlock(testPath, nil)
	if err != nil {
		t.Fatalf("Error opening storage: %v", err)
	}

	// Test the ObjectStore
	testObjectStore(t, block)
}

func TestObjectStoreMemory(t *testing.T) {
	// Open MemoryStorage
	mem := storage.OpenMemory(0, false)

	// Test the ObjectStore
	testObjectStore(t, mem)
}

func testObjectStore(t *testing.T, st storage.Storage) {
	expected := map[string]object.Object{
		"test1": newObjectf("hello world!"),
		"test2": newObjectf("hello world!\nhelloworld!\nhelloworld!\nhewwoooo wooowddd\n"),
		"test3": newObjectf("i dunno some random data here %d", 64),
		"test4": newObjectf("aaaaaaaaaaaaaaaa asfj;asdflkjasd;kfnasdklf"),
	}

	// Open ObjectStore
	obj, err := object.OpenStorage(st, 0)
	if err != nil {
		t.Fatalf("Error opening store: %v", err)
	}

	// Write bytes first to give something for later reads
	t.Run("ObjectStore.Put()", func(t *testing.T) {
		testObjectStorePut(t, obj, "test1", expected["test1"])
		testObjectStorePut(t, obj, "test2", expected["test2"])
		testObjectStorePut(t, obj, "test3", expected["test3"])
		testObjectStorePut(t, obj, "test4", expected["test4"])
	})

	// Now attempt to read from previous tests
	t.Run("ObjectStore.Get()", func(t *testing.T) {
		testObjectStoreGet(t, obj, "test1", expected["test1"])
		testObjectStoreGet(t, obj, "test2", expected["test2"])
		testObjectStoreGet(t, obj, "test3", expected["test3"])
		testObjectStoreGet(t, obj, "test4", expected["test4"])
	})

	// Check that files from previous tests exist
	t.Run("ObjectStore.Has()", func(t *testing.T) {
		for key := range expected {
			testObjectStoreHas(t, obj, key)
		}
	})

	// Attempt to remove 1 of files from previous test
	t.Run("ObjectStore.Delete()", func(t *testing.T) {
		testObjectStoreDelete(t, obj, "test1")
		delete(expected, "test1")
	})

	// Iterate keys and check expected
	t.Run("ObjectStore.Iterator()", func(t *testing.T) {
		testObjectStoreIterator(t, obj, expected)
	})

	// TODO: implement
	// TODO: concurrency tests
	t.Run("ObjectStore.Read()", func(t *testing.T) {})
	t.Run("ObjectStore.Update()", func(t *testing.T) {})

	// Check closing store
	t.Run("ObjectStore.Close()", func(t *testing.T) {
		testObjectStoreClose(t, obj)
	})
}

func testObjectStoreGet(t *testing.T, obj *object.ObjectStore, key string, expect object.Object) {
	v := testObject{}
	err := obj.Get(key, &v)
	if err != nil {
		t.Fatalf("Error getting object from store: %v", err)
	}
	err = equalObjects(expect, &v)
	if err != nil {
		t.Fatalf("Error getting expected object from store: %v", err)
	}
}

func testObjectStorePut(t *testing.T, obj *object.ObjectStore, key string, value object.Object) {
	err := obj.Put(key, value)
	if err != nil {
		t.Fatalf("Error putting object in store: %v", err)
	}
}

func testObjectStoreHas(t *testing.T, obj *object.ObjectStore, key string) {
	ok, err := obj.Has(key)
	if err != nil {
		t.Fatalf("Error checking if key in store: %v", err)
	}
	if !ok {
		t.Fatalf("Error checking expected key in store '%s'", key)
	}
}

func testObjectStoreDelete(t *testing.T, obj *object.ObjectStore, key string) {
	err := obj.Delete(key)
	if err != nil {
		t.Fatalf("Error deleting key from store: %v", err)
	}
}

func testObjectStoreIterator(t *testing.T, obj *object.ObjectStore, expected map[string]object.Object) {
	iter, err := obj.Iterator(nil)
	if err != nil {
		t.Fatalf("Error getting store iterator: %v", err)
	}

	count := 0
	for iter.Next() {
		key := iter.Key()
		if _, ok := expected[key]; !ok {
			t.Fatalf("Error finding unexpected key in store '%s'", key)
		}
		v := testObject{}
		err := iter.Value(&v)
		if err != nil {
			t.Fatalf("Error getting object from store iterator: %v", err)
		}
		err = equalObjects(&v, expected[key])
		if err != nil {
			t.Fatalf("Error checking key with '%s' has expected object: %v", key, err)
		}
		count++
	}

	if count != len(expected) {
		t.Fatalf("Unexpected number of keys in store '%d': '%v'", count, expected)
	}
}

func testObjectStoreClose(t *testing.T, obj *object.ObjectStore) {
	err := obj.Close()
	if err != nil {
		t.Fatalf("Error closing store: %v", err)
	}
	_, err = obj.Has("doesn't matter")
	if err != storage.ErrClosed {
		t.Fatalf("Did not return ErrClosed after storage closed: %v", err)
	}
}

type testObject struct{ s string }

func equalObjects(obj1, obj2 object.Object) error {
	o1, ok := obj1.(*testObject)
	if !ok {
		return errors.New("obj1 is of incorrect type")
	}
	o2, ok := obj2.(*testObject)
	if !ok {
		return errors.New("obj2 is of incorrect type")
	}
	if o1.s != o2.s {
		return errors.New("object strings do not match: '" + o1.s + "' '" + o2.s + "'")
	}
	return nil
}

func newObjectf(s string, a ...interface{}) object.Object {
	return &testObject{s: fmt.Sprintf(s, a...)}
}

func (obj *testObject) Copy() object.Object {
	return &testObject{s: obj.s}
}

func (obj *testObject) Set(obj2 object.Object) error {
	if obj2, ok := obj2.(*testObject); !ok {
		return errors.New("obj2 not of type *testObject")
	} else {
		obj.s = obj2.s
		return nil
	}
}

func (obj *testObject) Marshal() ([]byte, error) {
	return bytes.StringToBytes(obj.s), nil
}

func (obj *testObject) Unmarshal(b []byte) error {
	obj.s = string(b)
	return nil
}
