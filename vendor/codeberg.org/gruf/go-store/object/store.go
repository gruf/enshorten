package object

import (
	"time"

	"codeberg.org/gruf/go-cache"
	"codeberg.org/gruf/go-mutexes"
	"codeberg.org/gruf/go-store/storage"
)

// ObjectStore provides a key-value store for serializable objects, persisting their serialized
// form to disk in the same manner that KVStore performs, but storing deserialized objects in a
// TTL cache to reduce the performance impact of constant serializing / deserializing
type ObjectStore struct {
	cache   cache.Cache      // cache is the object TTL cache
	mutex   mutexes.MutexMap // mutex is a map of keys to mutexes to protect file access
	storage storage.Storage  // storage is the underlying storage
}

func OpenFile(path string, cfg *storage.DiskConfig, ttl time.Duration) (*ObjectStore, error) {
	// Attempt to open disk storage
	storage, err := storage.OpenFile(path, cfg)
	if err != nil {
		return nil, err
	}

	// Return new ObjectStore
	return OpenStorage(storage, ttl)
}

func OpenBlock(path string, cfg *storage.BlockConfig, ttl time.Duration) (*ObjectStore, error) {
	// Attempt to open block storage
	storage, err := storage.OpenBlock(path, cfg)
	if err != nil {
		return nil, err
	}

	// Return new Object store
	return OpenStorage(storage, ttl)
}

func OpenStorage(storage storage.Storage, ttl time.Duration) (*ObjectStore, error) {
	// Perform initial storage clean
	err := storage.Clean()
	if err != nil {
		return nil, err
	}

	// Prepare the cache
	cache := cache.New()
	if ttl > 0 {
		cache.SetTTL(ttl, false)
	}

	// Return new Object store
	return &ObjectStore{
		cache:   cache,
		mutex:   mutexes.NewMap(-1),
		storage: storage,
	}, nil
}

// RLock acquires a read-lock on supplied key, returning unlock function.
func (st *ObjectStore) RLock(key string) (runlock func()) {
	return st.mutex.RLock(key)
}

// Lock acquires a write-lock on supplied key, returning unlock function.
func (st *ObjectStore) Lock(key string) (unlock func()) {
	return st.mutex.Lock(key)
}

// Get will fetch and load the object at key into the supplied destination obj
func (st *ObjectStore) Get(key string, dst Object) error {
	return st.get(st.RLock, key, dst)
}

func (st *ObjectStore) getNoCache(rlock func(string) func(), key string, dst Object) error {
	// Get key read lock
	unlock := rlock(key)
	defer unlock()

	// Check store cache
	v, ok := st.cache.Get(key)
	if ok {
		// Object found, cast and set
		return dst.Set(v.(Object))
	} else {
		// Read file bytes
		b, err := st.storage.ReadBytes(key)
		if err != nil {
			return err
		}

		// Unmarshal obj bytes
		return dst.Unmarshal(b)
	}
}

func (st *ObjectStore) get(rlock func(string) func(), key string, dst Object) error {
	// Get key read lock
	unlock := rlock(key)
	defer unlock()

	// Check store cache
	v, ok := st.cache.Get(key)
	if ok {
		// Object found, cast and set
		return dst.Set(v.(Object))
	} else {
		// Read file bytes
		b, err := st.storage.ReadBytes(key)
		if err != nil {
			return err
		}

		// Unmarshal obj bytes
		err = dst.Unmarshal(b)
		if err != nil {
			return err
		}

		// Place in cache
		st.cache.Put(key, dst.Copy())
		return nil
	}
}

// Put will place the supplied source object into the store at the key location
func (st *ObjectStore) Put(key string, src Object) error {
	return st.put(st.Lock, key, src)
}

func (st *ObjectStore) put(lock func(string) func(), key string, src Object) error {
	// Get key write lock
	unlock := st.Lock(key)
	defer unlock()

	// Check if in cache
	v, ok := st.cache.Get(key)
	if ok {
		// Cast and update
		return v.(Object).Set(src)
	} else {
		// Marshal bytes
		b, err := src.Marshal()
		if err != nil {
			return err
		}

		// Write file bytes
		err = st.storage.WriteBytes(key, b)
		if err != nil {
			return err
		}

		// Place in cache
		st.cache.Put(key, src.Copy())
		return nil
	}
}

// Has checks if a key exists in the store
func (st *ObjectStore) Has(key string) (bool, error) {
	return st.has(st.RLock, key)
}

func (st *ObjectStore) has(rlock func(string) func(), key string) (bool, error) {
	// Get key read lock
	runlock := st.RLock(key)
	defer runlock()

	// Check cache for key
	if st.cache.Has(key) {
		return true, nil
	}

	// Else check on disk
	return st.storage.Stat(key)
}

// Delete removes the key-object pair from the store
func (st *ObjectStore) Delete(key string) error {
	return st.delete(st.Lock, key)
}

func (st *ObjectStore) delete(lock func(string) func(), key string) error {
	// Acquire key write lock
	unlock := st.Lock(key)
	defer unlock()

	// Remove key from cache
	st.cache.Invalidate(key)

	// Remove from filesystem
	return st.storage.Remove(key)
}

// Iterator returns an Iterator for key-object pairs in the store, using supplied match function
func (st *ObjectStore) Iterator(matchFn func(string) bool) (*ObjectIterator, error) {
	// If no function, match all
	if matchFn == nil {
		matchFn = func(string) bool { return true }
	}

	// Get store read lock
	state := st.mutex.RLockMap()

	// Setup the walk keys function
	entries := []storage.StorageEntry{}
	walkFn := func(entry storage.StorageEntry) {
		// Ignore unmatched entries
		if !matchFn(entry.Key()) {
			return
		}

		// Add to entries
		entries = append(entries, entry)
	}

	// Walk keys in the storage
	err := st.storage.WalkKeys(storage.WalkKeysOptions{WalkFn: walkFn})
	if err != nil {
		state.UnlockMap()
		return nil, err
	}

	// Return new iterator
	return &ObjectIterator{
		store:   st,
		state:   state,
		entries: entries,
		index:   -1,
		key:     "",
	}, nil
}

func (st *ObjectStore) Read() *StateRO {
	state := st.mutex.RLockMap()
	return &StateRO{store: st, state: state}
}

func (st *ObjectStore) ReadFn(fn func(*StateRO)) {
	// Acquire read-only state
	state := st.Read()
	defer state.Release()

	// Pass to fn
	fn(state)
}

func (st *ObjectStore) Update() *StateRW {
	state := st.mutex.LockMap()
	return &StateRW{store: st, state: state}
}

func (st *ObjectStore) UpdateFn(fn func(*StateRW)) {
	// Acquire read-write state
	state := st.Update()
	defer state.Release()

	// Pass to fn
	fn(state)
}

// Close will close the underlying storage, the mutex map locking (e.g. RLock(), Lock() will still work).
func (st *ObjectStore) Close() error {
	return st.storage.Close()
}
