package object

import (
	"codeberg.org/gruf/go-errors"
	"codeberg.org/gruf/go-mutexes"
	"codeberg.org/gruf/go-store/storage"
)

var ErrIteratorClosed = errors.New("store/object: iterator closed")

// ObjectIterator provides an iterator to all the key-value
// pairs in an ObjectStore. While the iterator is open the store will be
// read locked, you MUST release the iterator when you are finished with
// it.
//
// Please note:
// - as this iterator only acquires a store read-lock, any values loaded
// from disk will NOT be cached
// - individual iterators are NOT concurrency safe, though it is safe to
// have multiple iterators running concurrently
type ObjectIterator struct {
	store   *ObjectStore // store is the linked ObjectStore
	state   *mutexes.LockState
	entries []storage.StorageEntry
	index   int
	key     string
}

// Next attempts to set the next key-value pair, the
// return value is if there was another pair remaining
func (i *ObjectIterator) Next() bool {
	next := i.index + 1
	if next >= len(i.entries) {
		i.key = ""
		return false
	}
	i.key = i.entries[next].Key()
	i.index = next
	return true
}

// Key returns the next key from the store
func (i *ObjectIterator) Key() string {
	return i.key
}

// Release releases the ObjectIterator and ObjectStore's read lock
func (i *ObjectIterator) Release() {
	i.state.UnlockMap()
	i.store = nil
	i.key = ""
	i.entries = nil
}

// Value attempts to load the next object in the ObjectStore into the supplied destination
func (i *ObjectIterator) Value(dst Object) error {
	// Check store isn't closed
	if i.store == nil {
		return ErrIteratorClosed
	}

	// Attempt to get obj
	return i.store.getNoCache(i.state.RLock, i.key, dst)
}
