package object

// Object defines a (de)serializable object
type Object interface {
	// Copy should return a copy of the current object
	Copy() Object

	// Set should attempt to set the current object details to the supplied
	Set(Object) error

	// Marshal should attempt to serialize a binary representation of the object
	Marshal() ([]byte, error)

	// Unmarshal should attempt to deserialize from a binary representation
	Unmarshal([]byte) error
}
