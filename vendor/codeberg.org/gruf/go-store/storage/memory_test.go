package storage_test

import (
	"testing"

	"codeberg.org/gruf/go-store/storage"
)

func TestMemoryStorage(t *testing.T) {
	// Open new memorystorage instance
	st := storage.OpenMemory(0, false)

	// Run the storage tests
	testStorage(t, st)
}
