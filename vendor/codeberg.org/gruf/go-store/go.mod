module codeberg.org/gruf/go-store

go 1.14

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-cache v1.1.2
	codeberg.org/gruf/go-errors v1.0.5
	codeberg.org/gruf/go-fastpath v1.0.2
	codeberg.org/gruf/go-hashenc v1.0.1
	codeberg.org/gruf/go-mutexes v1.1.0
	codeberg.org/gruf/go-nowish v1.1.0 // indirect
	codeberg.org/gruf/go-pools v1.0.2
	github.com/golang/snappy v0.0.4
)
