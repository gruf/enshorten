Two different kinds of stores available:

    - KVStore: a simple, highly perform key-value store that writes straight to disk

    - ObjectStore: a key-value store for serializable objects, changes are written directly to disk and data structures are stored in an LRU cache to minimise the amount of serialization / deserialization required

The underlying storage method for these stores can be handled by:

    - DiskStorage: storage implementation that stores directly to a filesystem, with optional settable key <--> filepath transform function

    - BlockStorage: storage implementation that stores by chunking values and storing chunks under their respective hashes

    - MemoryStorage: storage implementation that is just an in-memory `map[string][]byte`

DiskStorage and BlockStorage support compression by implementing a `Compressor` interface, with default gzip and zlib implementations.