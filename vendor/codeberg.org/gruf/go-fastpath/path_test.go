package fastpath_test

import (
	stdpath "path"
	"testing"

	"codeberg.org/gruf/go-fastpath"
)

const pathBufSize = 256

var testPaths = []string{
	"",
	"home/user",
	"/home/user",
	"/root",
	"/home/user/../user2",
	"../../ham",
	"../../../../../../../../",
	".",
	"//",
	"/home/user/Downloads/folder/../../",
	"home/user/../../var/opt/../../home/user/Downloads/../../../../../root/",
	"/.",
	"reallylongnamestringaaaaaaaaaaaa",
	"/m",
	"m/",
	"/m/",
	"/./",
	"./",
	"/../",
	"../",
	"/..",
	"/.bashrc",
	".bashrc",
	".e",
	"e.",
	"../path/../path/../path/../path/../path/../path/../",
	"/path/../path/../path/../path/../path/../path/../path/",
}

var (
	benchPath1 = "/home/user/Downloads/folder/../../"
	benchPath2 = "home/user/../../var/opt/../../home/user/Downloads/../../../../../root/"
)

func testClean(t *testing.T, p string) {
	builder := fastpath.NewBuilder(make([]byte, 0, 512))

	t.Logf("testClean: %s", p)
	test := builder.Clean(p)
	expect := stdpath.Clean(p)
	if test != expect {
		t.Errorf("Failed: expect = \"%s\", result = \"%s\"", expect, test)
	}
}

func testJoin(t *testing.T, p1, p2 string) {
	builder := fastpath.NewBuilder(make([]byte, 0, 512))

	t.Logf("testJoin: %s %s", p1, p2)
	test := builder.Join(p1, p2)
	expect := stdpath.Join(p1, p2)
	if test != expect {
		t.Errorf("Failed: expect = \"%s\", result = \"%s\"", expect, test)
	}
}

func testAbsolute(t *testing.T, p string) {
	builder := fastpath.NewBuilder(make([]byte, 0, 512))

	t.Logf("testAbsolute: %s", p)
	builder.AppendString(p)
	path := stdpath.Clean(p)
	test := builder.Absolute()
	expect := stdpath.IsAbs(path)
	if test != expect {
		t.Errorf("Failed: expect = \"%v\", result = \"%v\" -- %s", expect, test, builder.StringPtr())
	}
}

func testSetAbsolute(t *testing.T, p string) {
	builder := fastpath.NewBuilder(make([]byte, 0, 512))

	t.Logf("testAbsolute: %s", p)
	builder.AppendString(p)

	before := builder.Absolute()
	if before {
		builder.SetAbsolute(false)
	} else {
		builder.SetAbsolute(true)
	}

	test := builder.Absolute()
	expect := stdpath.IsAbs(builder.StringPtr())

	if test != expect {
		t.Errorf("Failed: expect = \"%v\", result = \"%v\" -- %s", expect, test, builder.StringPtr())
	}
}

// func testDirname(t *testing.T, p string) {
// 	builder := pool.Get()
// 	defer pool.Put(builder)

// 	t.Logf("testDirname")
// 	builder.Append(p)
// 	test := builder.Dirname()
// 	expect := stdpath.Dir(p)
// 	if test != expect {
// 		t.Errorf("Failed: expect = \"%s\", result = \"%s\"", expect, test)
// 	}
// }

func TestClean(t *testing.T) {
	for _, p := range testPaths {
		testClean(t, p)
	}
}

func TestJoin(t *testing.T) {
	for _, p1 := range testPaths {
		for _, p2 := range testPaths {
			testJoin(t, p1, p2)
		}
	}
}

func TestAbsolute(t *testing.T) {
	for _, p := range testPaths {
		testAbsolute(t, p)
	}
}

func TestSetAbsolute(t *testing.T) {
	for _, p := range testPaths {
		testSetAbsolute(t, p)
	}
}

// func TestPathDirname(t *testing.T) {
// 	for _, p := range testPaths {
// 		testDirname(t, p)
// 	}
// }

func BenchmarkBuilderClean(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		bldr := fastpath.NewBuilder(make([]byte, pathBufSize))
		for pb.Next() {
			bldr.Clean(benchPath1)
			bldr.Clean(benchPath2)
		}
	})
}

func BenchmarkStdPathClean(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			stdpath.Clean(benchPath1)
			stdpath.Clean(benchPath2)
		}
	})
}

func BenchmarkBuilderJoin(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		bldr := fastpath.NewBuilder(make([]byte, pathBufSize))
		for pb.Next() {
			bldr.Join(benchPath1, benchPath2)
			bldr.Join(benchPath1, benchPath2)
		}
	})
}

func BenchmarkStdPathJoin(b *testing.B) {
	b.ReportAllocs()
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			stdpath.Join(benchPath1, benchPath2)
			stdpath.Join(benchPath1, benchPath2)
		}
	})
}
