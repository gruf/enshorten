package mutexes_test

import (
	"strconv"
	"sync"
	"testing"
	"time"

	"codeberg.org/gruf/go-mutexes"
)

const hammerTestCount = 10000

func TestMapMutex(t *testing.T) {
	mm := mutexes.NewMap(-1)

	runlock1 := mm.RLock("r-hello")
	runlock2 := mm.RLock("r-hello")
	runlock3 := mm.RLock("r-hello")

	runlock1()
	runlock2()
	runlock3()

	unlock := mm.Lock("w-hello")

	unlock()
}

func TestMapMutexRLock(t *testing.T) {
	mm := mutexes.NewMap(-1)

	// Acquire map-wide read lock
	state := mm.RLockMap()

	// Acquire read locks for key
	runlock1 := state.RLock("r-hello")
	runlock2 := state.RLock("r-hello")
	runlock3 := state.RLock("r-hello")

	// Check other reads work
	runlock4 := mm.RLock("r-hello")

	// Unlock all
	runlock1()
	runlock2()
	runlock3()
	runlock4()

	// Unlock map
	state.UnlockMap()

	// Check write goes through
	unlock := mm.Lock("w-hello")
	unlock()
}

func TestMapMutexLock(t *testing.T) {
	mm := mutexes.NewMap(-1)

	// Acquire map-wide write lock
	state := mm.LockMap()

	// Acquire write locks on keys
	unlock1 := state.Lock("w-hello1")
	unlock2 := state.Lock("w-hello2")
	unlock3 := state.Lock("w-hello3")

	// Acquire read lock on keys
	runlock1 := state.RLock("r-hello")
	runlock2 := state.RLock("r-hello")
	runlock3 := state.RLock("r-hello")

	// Unlock all
	unlock1()
	unlock2()
	unlock3()
	runlock1()
	runlock2()
	runlock3()

	// Unlock map
	state.UnlockMap()

	// Check write on keys
	unlock1 = mm.Lock("w-hello1")
	unlock2 = mm.Lock("w-hello2")
	unlock3 = mm.Lock("w-hello3")

	unlock1()
	unlock2()
	unlock3()
}

func TestMapMultiLocks(t *testing.T) {
	mm := mutexes.NewMap(-1)

	// Acquire map-wide read lock
	state := mm.RLockMap()

	// Acquire read locks on keys
	runlock1 := state.RLock("r-hello")
	runlock2 := state.RLock("r-hello")
	runlock3 := state.RLock("r-hello")

	ch := make(chan struct{})

	// Block waiting for write
	go func() {
		ch <- struct{}{}
		unlock := mm.Lock("w-hello")
		ch <- struct{}{}
		ch <- struct{}{}
		unlock()
	}()

	// Wait on parallel
	<-ch

	// Ensure write NOT gone through
	// NOTE: this sleep here is not exact,
	// we're just hoping that a write lock
	// attempt would have gone through in
	// this time, though ultimately we have
	// no control over the go scheduler or
	// orders of waking sleeping goroutines
	time.Sleep(time.Millisecond)
	select {
	case <-ch:
		t.Fatal("write locked achieved during RLockMap()")
	default:
	}

	// Check other reads work
	runlock4 := mm.RLock("r-hello")

	// Unlock all reads
	runlock1()
	runlock2()
	runlock3()
	runlock4()

	// Unlock map
	state.UnlockMap()

	// Write should have worked
	time.Sleep(time.Millisecond)
	select {
	case <-ch:
	default:
		t.Fatal("waiting write failed after unlockMap()")
	}

	ch2 := make(chan struct{})

	// BEFORE unlocking, get LockMap()
	go func() {
		ch2 <- struct{}{}
		state := mm.LockMap()
		ch2 <- struct{}{}
		unlock := state.Lock("w-hello")
		ch2 <- struct{}{}
		ch2 <- struct{}{}

		ch3 := make(chan struct{})
		go func() {
			ch3 <- struct{}{}
			state.UnlockMap()
			ch3 <- struct{}{}
		}()

		// triger unlock map
		<-ch3

		// UnlockMap() should failed
		// until unlock()
		time.Sleep(time.Millisecond)
		select {
		case <-ch3:
			panic("unlocked map with key lock still active")
		default:
		}

		// Unlock key
		unlock()

		// Ensure unlock map gone through
		time.Sleep(time.Millisecond)
		select {
		case <-ch3:
		default:
			panic("map did not unlock after key released")
		}
	}()

	// Wait on parallel
	<-ch2

	// Ensure LockMap() gone through
	time.Sleep(time.Millisecond)
	select {
	case <-ch2:
		t.Fatal("LockMap() achieved with existing write lock")
	default:
	}

	// Release write
	<-ch

	// Ensure lockmap works
	time.Sleep(time.Millisecond)
	select {
	case <-ch2:
	default:
		t.Fatal("LockMap() failed on unlocked map")
	}

	// Check write lock worked
	time.Sleep(time.Millisecond)
	select {
	case <-ch2:
	default:
		t.Fatal("waiting write lock failed")
	}
}

func TestMapMutexHammer(t *testing.T) {
	mm := mutexes.NewMap(-1)

	t.Log("it's hammer time!") // haha

	wg := sync.WaitGroup{}
	for i := 0; i < hammerTestCount; i++ {
		go func(i int) {
			key := strconv.Itoa(i)

			// Get the original starting lock
			runlockBase := mm.RLock(key)

			// Perform slow async unlock
			wg.Add(1)
			go func() {
				time.Sleep(time.Second)
				runlockBase()
				wg.Done()
			}()

			for j := 0; j < hammerTestCount; j++ {
				runlock := mm.RLock(key)

				// Perform slow async unlock
				wg.Add(1)
				go func() {
					time.Sleep(time.Millisecond * 10)
					runlock()
					wg.Done()
				}()
			}
		}(i)
	}
	time.Sleep(time.Second)
	wg.Wait()
}
