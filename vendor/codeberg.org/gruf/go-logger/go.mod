module codeberg.org/gruf/go-logger

go 1.14

require (
	codeberg.org/gruf/go-format v1.0.3
	codeberg.org/gruf/go-nowish v1.1.0
	github.com/sirupsen/logrus v1.8.1
)
