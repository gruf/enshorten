module codeberg.org/gruf/go-middleware

go 1.17

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-logger v1.4.1
	github.com/valyala/fasthttp v1.31.0
)

require (
	codeberg.org/gruf/go-format v1.0.3 // indirect
	codeberg.org/gruf/go-nowish v1.1.0 // indirect
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
