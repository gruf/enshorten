package middleware

import (
	"runtime"
	"strconv"
	"time"

	"codeberg.org/gruf/go-logger"
	"github.com/valyala/fasthttp"
)

// Logger wraps a fasthttp.RequestHandler to add request logging to specified logger.
func Logger(log *logger.Logger, handler fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		// Get before-request time
		before := time.Now()

		defer func() {
			// Get request timings
			diff := time.Since(before)

			// Post request log entry
			entry := log.Entry().WithContext(ctx)
			entry.TimestampIf().Level(logger.INFO)
			entry.Fields(
				logger.KV{K: "method", V: b2s(ctx.Request.Header.Method())},
				logger.KV{K: "path", V: b2s(ctx.URI().Path())},
				logger.KV{K: "status", V: ctx.Response.StatusCode()},
				logger.KV{K: "latency", V: diff},
			)

			// Finally, log message
			entry.Msg("http request")
		}()

		// Perform handler
		handler(ctx)
	}
}

// Recovery wraps a fasthttp.RequestHandler to add panic recovery and handling via HTTPPanicHandler().
func Recovery(log *logger.Logger, handler fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(ctx *fasthttp.RequestCtx) {
		defer func() {
			if r := recover(); r != nil {
				PanicHandler(log, ctx, r)
			}
		}()
		handler(ctx)
	}
}

// PanicHandler will handle the result of a panic during fasthttp.RequestHandler, logging
// a stracktrace and contents of the recovered interface to the given logger at error level.
func PanicHandler(log *logger.Logger, ctx *fasthttp.RequestCtx, i interface{}) {
	if ctx.Response.StatusCode() == 0 {
		// If panic with no response, ensure to send one
		ctx.Error("Internal Server Error", fasthttp.StatusInternalServerError)
	}

	// Prepare a stacktrace
	pcs := make([]uintptr, 10)
	depth := runtime.Callers(2, pcs)
	frames := runtime.CallersFrames(pcs[:depth])

	// Start the log entry
	entry := log.Entry().WithContext(ctx)
	entry.TimestampIf().Level(logger.ERROR)

	// Write stacktrace frames to Entry buffer
	for f, again := frames.Next(); again; f, again = frames.Next() {
		entry.Append("\t" + f.File + " #" + strconv.Itoa(f.Line) + ": " + f.Function)
	}

	// Log with request ID at ERROR level
	entry.Msg("caught panic: ", i)
}
