package middleware

import "codeberg.org/gruf/go-bytes"

// b2s is shorthand for BytesToString().
func b2s(b []byte) string {
	return bytes.BytesToString(b)
}
