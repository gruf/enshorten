package runners_test

import (
	"context"
	"testing"
	"time"

	"codeberg.org/gruf/go-runners"
)

func TestService(t *testing.T) {
	ch := make(chan struct{})
	svc := &runners.Service{}

	t.Log("Checking if svc running")
	if svc.Running() {
		t.Fatal("unstarted Service marked as running")
	}

	t.Log("Check svc Done() never nil")
	if svc.Done() == nil {
		t.Fatal("Service done channel came back nil")
	}

	go func(t *testing.T) {
		t.Log("Starting svc in non-blocking goroutine")
		svc.Run(func(ctx context.Context) {
			ch <- struct{}{}
			t.Log("started svc routine")
			<-ctx.Done()
			t.Log("stopped svc routine")
		})
	}(t)

	t.Log("Starting .Run() timer")
	timeout := time.NewTimer(time.Second * 30)

	select {
	case <-ch:
		t.Log("Started running")
		if !timeout.Stop() {
			<-timeout.C
		}

	case <-timeout.C:
		t.Fatal("failed to start after 30s")
	}

	t.Log("Checking svc state is running")
	if !svc.Running() {
		t.Fatal("started Service marked as not running")
	}

	done := svc.Done()
	go func(t *testing.T) {
		t.Log("Stopping svc in non-blocking goroutine")
		svc.Stop()
	}(t)

	t.Log("Starting .Stop() timer")
	timeout.Reset(time.Second * 30)

	select {
	case <-done:
		t.Log("Stopping running")
		if !timeout.Stop() {
			<-timeout.C
		}

	case <-timeout.C:
		t.Fatal("failed to stop after 30s")
	}

	t.Log("Checking svc state isn't running")
	if svc.Running() {
		t.Fatal("stopping Service marked as running")
	}

	t.Log("Attempting 2nd (expected failing) .Stop()")
	if svc.Stop() {
		t.Fatal("stopped Service already in stopping state")
	}
}

func TestServiceReuse(t *testing.T) {
	svc := runners.Service{}
	for i := 0; i < 1000; i++ {
		check := false
		haveRun := svc.Run(func(ctx context.Context) {
			check = true
		})
		if !check || !haveRun {
			t.Fatal("failed check")
		}
	}
}

func TestServiceDone(t *testing.T) {
	svc := runners.Service{}
	sync := make(chan struct{})

	done := svc.Done()
	go func() {
		sync <- struct{}{}
		<-done
		sync <- struct{}{}
	}()
	<-sync

	t.Log("Checking .Done() channel hasn't prematurely closed")
	select {
	case <-sync:
		t.Fatal("service done channel closed unexpectedly")
	default:
	}

	t.Log("Attempting to start service")
	did := svc.Run(nil)
	if !did {
		t.Fatal("failed to start service")
	}

	t.Log("Checking done channel closed")
	<-sync
	<-done
}
