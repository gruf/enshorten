package runners_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"codeberg.org/gruf/go-runners"
)

func TestWorkerPool(t *testing.T) {
	testWorkerPool(t, 10, 100)
	testWorkerPool(t, 100, 1000)
}

func testWorkerPool(t *testing.T, workers int, queue int) {
	pool := runners.NewWorkerPool(workers, queue)

	// Attempt to start the pool
	if !pool.Start() {
		t.Fatal("failed to start WorkerPool")
	}

	// Used to synchronize tasks
	tasker := sync.WaitGroup{}
	all := sync.WaitGroup{}

	// Create 1st context for this execution
	ctx1, cncl1 := runners.ContextWithCancel()
	defer cncl1()

	// Enqueue tasks to saturate workers
	for i := 0; i < workers; i++ {
		x := i
		all.Add(1)
		tasker.Add(1)
		pool.Enqueue(func(ctx context.Context) {
			tasker.Done()
			t.Logf("running task: %d", x)
			select {
			case <-ctx1.Done():
			case <-ctx.Done():
			}
			all.Done()
			t.Logf("task done: %d: ", x)
		})
	}

	// Ensure worker count at max
	tasker.Wait()
	if pool.Workers() != workers {
		t.Fatal("failed to saturate WorkerPool workers")
	}

	// Now fill up the rest of the queue slots
	for i := 0; i < queue+1; i++ {
		x := i
		all.Add(1)
		pool.Enqueue(func(ctx context.Context) {
			t.Logf("running task: %d", x)
			select {
			case <-ctx1.Done():
			case <-ctx.Done():
			}
			all.Done()
			t.Logf("task done: %d: ", x)
		})
	}

	// Give time to queue, check is queued
	time.Sleep(time.Second) // this is not exact...
	if pool.Queue() != queue {
		t.Fatal("failed to saturate WorkerPool queue")
	}

	// This next queue should now fail
	if pool.EnqueueNoBlock(func(ctx context.Context) {}) {
		t.Fatal("queued WorkerFunc when WorkerPool queue saturated")
	}

	// Now cancel all tasks and wait
	if !pool.Stop() {
		t.Fatal("failed to stop WorkerPool")
	}
	all.Wait()
}
