module codeberg.org/gruf/go-cache

go 1.17

require (
	codeberg.org/gruf/go-nowish v1.0.0
	codeberg.org/gruf/go-runners v1.1.1
	github.com/google/go-cmp v0.5.6
)
