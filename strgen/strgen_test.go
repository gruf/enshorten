package strgen_test

import (
	"testing"

	"codeberg.org/gruf/enshorten/strgen"
)

func TestRandomString(t *testing.T) {
	for i := 1; i < 1000; i++ {
		// Generate random string
		str := strgen.RandomString(uint(i))

		// Check is expected length
		if len(str) != i {
			t.Fatal("random string not of expected length")
		}

		// Check only expected chars
		for _, c := range str {
			in := false
			for _, rc := range strgen.RandomChars {
				if rc == c {
					in = true
				}
			}
			if !in {
				t.Fatal("random string contains unexpected chars")
			}
		}
	}
}
