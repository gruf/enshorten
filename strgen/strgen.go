package strgen

import (
	crand "crypto/rand"
	"math/big"
	"math/rand"
	"strings"
	"time"
)

const (
	// RandomChars is the charset used when generating a random string.
	RandomChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	// RandomChars length var for prettier code.
	randomCharsLen = int64(len(RandomChars))
)

func init() {
	// Get cur unix nano time
	nano := time.Now().UnixNano()

	// Use an actual random source to help seed fast-rand
	n, err := crand.Int(crand.Reader, big.NewInt(nano))
	if err != nil {
		panic(err)
	}

	// Set the fast rand seed
	rand.Seed(nano + n.Int64())
}

// RandomString returns a new random string of requested length. Note that this is
// generated using math/rand and NOT crypto/rand so it is not suitable for situations
// requiring cryptographically secure random strings.
func RandomString(length uint) string {
	buf := strings.Builder{}
	for i := uint(0); i < length; i++ {
		// Get next index of random chars string to use
		idx := rand.Int63n(randomCharsLen)

		// Write next byte from random chars str
		buf.WriteByte(RandomChars[idx])
	}
	return buf.String()
}
