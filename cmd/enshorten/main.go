package main

import (
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"codeberg.org/gruf/enshorten"
	"codeberg.org/gruf/enshorten/store"
	"codeberg.org/gruf/go-logger"
	"github.com/spf13/pflag"
	"github.com/valyala/fasthttp"
)

// Compile-time set version information.
var (
	Version = ""
	Commit  = ""
)

func main() {
	// Prepare configuration
	var storePath, listenAddr, tlsCrt, tlsKey string
	var urlAge, cleanFreq time.Duration
	cfg := enshorten.Config{}
	var help, version bool

	// Parse cmdline flags
	pflag.UintVarP(&cfg.KeyLen, "key-len", "", 8, "Generated shortened URL key length")
	pflag.StringVarP(&cfg.Address, "address", "a", "", "Server URL address")
	pflag.StringVarP(&storePath, "store", "p", ":memory:?size=100", "Persistent store fs path (:memory: = in-memory)")
	pflag.StringVarP(&listenAddr, "listen", "l", "127.0.0.1:8080", "HTTP listener bind address")
	pflag.StringVarP(&tlsCrt, "tls-cert", "c", "", "TLS certificate file")
	pflag.StringVarP(&tlsKey, "tls-key", "k", "", "TLS key file")
	pflag.DurationVarP(&urlAge, "max-age", "", time.Hour*24*365, "Shortened URL lifespan")
	pflag.DurationVarP(&cleanFreq, "frequency", "", time.Hour, "Expired short URL cleanup frequency")
	pflag.BoolVarP(&help, "help", "h", false, "Print usage string")
	pflag.BoolVarP(&version, "version", "v", false, "Print version string")
	pflag.Parse()

	switch {
	// Usage flag
	case help:
		fmt.Printf("Usage: %s <url> ...\n%s", os.Args[0], pflag.CommandLine.FlagUsages())
		os.Exit(0)

	// Version flag
	case version:
		fmt.Printf("Enshorten (%s-%s)\n", Version, Commit)
		os.Exit(0)
	}

	// Calculate if using TLS or not
	isTLS := (tlsCrt != "" && tlsKey != "")

	// If no addr provided, use bind addr
	if cfg.Address == "" {
		// we set scheme so url parse succeeds
		// (it doesn't matter, it gets reset below)
		if strings.HasPrefix(listenAddr, ":") {
			cfg.Address = "http://0.0.0.0" + listenAddr
		} else {
			cfg.Address = "http://" + listenAddr
		}
	}

	// Parse address before scheme setting
	addr, err := url.Parse(cfg.Address)
	if err != nil {
		logger.Fatal(err)
	}

	// Ensure addr contains a scheme
	switch isTLS {
	case true:
		addr.Scheme = "https"
	case false:
		addr.Scheme = "http"
	}

	// Set the checked address
	cfg.Address = addr.String()

	// Open the URL store
	logger.Infof("opening store: %s", storePath)
	store, err := store.Open(storePath)
	if err != nil {
		logger.Fatal(err)
	}

	// Open the application with config
	app := enshorten.New(store, cfg)

	// Prepare the HTTP server
	srv := fasthttp.Server{
		Handler: app.Handler,
	}

	// Start URL cleanup routine
	go func() {
		for {
			// Perform a shortened URL prune!
			count, err := store.Prune(urlAge)
			if err != nil {
				logger.Error(err)
			}

			// Log any pruned URLs
			if count > 0 {
				logger.Infof("pruned %d expired URLs", count)
			}

			// Sleep before next clean
			time.Sleep(cleanFreq)
		}
	}()

	// Start listening for OS signals
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		// Wait on OS signal
		sig := <-signals
		logger.Info("received signal: " + sig.String())

		// Shutdown HTTP
		_ = srv.Shutdown()
	}()

	switch isTLS {
	// Listen HTTPS using TLS cert + key
	case true:
		logger.Infof("listening on https://" + listenAddr)
		err = srv.ListenAndServeTLS(listenAddr, tlsCrt, tlsKey)

	// Listen HTTP
	case false:
		logger.Info("listening on http://" + listenAddr)
		err = srv.ListenAndServe(listenAddr)
	}

	// Check for final error
	if err != nil {
		logger.Fatal(err)
	}
}
