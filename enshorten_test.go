package enshorten_test

import (
	"fmt"
	"strings"
	"testing"

	"codeberg.org/gruf/enshorten"
	"codeberg.org/gruf/enshorten/store"
	"github.com/valyala/fasthttp"
)

func NewApp() (*enshorten.Enshorten, error) {
	store, err := store.Open(":memory:?size=100")
	if err != nil {
		return nil, err
	}
	return enshorten.New(
		store,
		enshorten.Config{
			KeyLen:  8,
			Address: "http://127.0.0.1:8080",
		},
	), nil
}

func TestEnshortenRoot(t *testing.T) {
	app, err := NewApp()
	if err != nil {
		t.Fatalf("failed creating app: %v", err)
	}

	// Prepare context for request
	ctx := fasthttp.RequestCtx{}
	ctx.Response.SetBody([]byte{})

	// Pass to app
	app.Root(&ctx)

	// Check status code
	if ctx.Response.StatusCode() != 200 {
		t.Fatal("unexpected status code")
	}

	// Prepare the test root help string
	rootHelp := fmt.Sprintf(`Enshorten -- a URL shortening service!

Usage:
$ curl https://%s/s?url=<URL>
--> '%s/<KEY>'
$ curl https://%s/<KEY>
--> redirects you to <URL>
`, "http://127.0.0.1:8080", "http://127.0.0.1:8080", "http://127.0.0.1:8080")

	// Check response expected
	body := string(ctx.Response.Body())
	if strings.TrimSpace(body) != strings.TrimSpace(rootHelp) {
		t.Fatal("root help page did not return expected")
	}
}

func TestEnshortenCreateFollowShortURL(t *testing.T) {
	app, err := NewApp()
	if err != nil {
		t.Fatalf("failed creating app: %v", err)
	}

	// Prepare context for request
	ctx := fasthttp.RequestCtx{}
	ctx.Response.SetBody([]byte{})

	// Create a test URL
	uri := ctx.Request.URI()
	uri.SetPath("/s")
	uri.SetQueryString("url=https://google.com")

	// Pass to create handler
	app.CreateShortURL(&ctx)

	// Check status code
	if ctx.Response.StatusCode() != 200 {
		t.Fatal("unexpected status code")
	}

	// We should have received 'http://127.0.0.1:8080/<key>'
	body := string(ctx.Response.Body())
	if len(body) != 30 || !strings.HasPrefix(body, "http://127.0.0.1:8080") {
		t.Fatal("returned shortened URL not as expected", body)
	}

	// Prepare new context for request
	ctx = fasthttp.RequestCtx{}
	ctx.Response.SetBody([]byte{})

	// Set the test URL to returned body
	uri = ctx.Request.URI()
	_ = uri.Parse(nil, []byte(body))
	ctx.SetUserValue(
		"key", // we have to manually set the path param
		strings.TrimPrefix(body, "http://127.0.0.1:8080/"),
	)

	// Pass to app
	app.FollowShortURL(&ctx)

	// Check status code
	if ctx.Response.StatusCode() != 302 {
		t.Fatal("unexpected status code")
	}

	// Get and check the location header
	location := ctx.Response.Header.Peek("Location")
	if string(location) != "https://google.com/" {
		t.Fatal("unexpected redirect destination")
	}
}
